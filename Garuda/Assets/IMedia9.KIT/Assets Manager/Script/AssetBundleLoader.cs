﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class AssetBundleLoader : MonoBehaviour
    {
        public string XMLManagerName;
        GameObject XMLManagerObject;

        [Header("Status Assets")]
        public bool isLoadAssets = false;

        [Header("Assets Manager")]
        public string AssetFile;
        public AssetBundle[] AssetManagers;

        [System.Serializable]
        public class CAssetsCollection
        {
            public bool isEnabled;
            public string AssetName;
            public GameObject AssetObject;
        }

        [Header("Assets Collection")]
        //public CAssetsCollection[] AssetsCollection;
        public List<CAssetsCollection> AssetsCollection;

        void Start()
        {
            XMLManagerObject = GameObject.Find(XMLManagerName);

            AssetsCollection = new List<CAssetsCollection>();
        }

        void LateUpdate()
        {
            if (XMLManagerObject != null && XMLManagerObject.GetComponent<XMLManager>().isLoaded)
            {
                if (!isLoadAssets)
                {
                    isLoadAssets = true;

                    UnitConsole.PrintDebug("AssetManagers(34): Initialize Array: " + XMLManagerObject.GetComponent<XMLManager>().AssetsBundleData.Length.ToString());
                    AssetManagers = new AssetBundle[XMLManagerObject.GetComponent<XMLManager>().AssetsBundleData.Length];

                    for (int i = 0; i < XMLManagerObject.GetComponent<XMLManager>().AssetsBundleData.Length; i++)
                    {
                        AssetFile = XMLManagerObject.GetComponent<XMLManager>().AssetsBundleData[i].FileName;
                        UnitConsole.PrintDebug("AssetBundleLoader(37): Load Assets: " + AssetFile);
                        LoadAssetBundles(i, Application.streamingAssetsPath + AssetFile);
                    }
                }
            }
        }

        public void ClearAllAssets()
        {
            for (int i=0; i< AssetsCollection.Count; i++)
            {
                Destroy(AssetsCollection[i].AssetObject);
            }
            AssetsCollection.Clear();
        }

        //-- LOAD ARRAY ASSETS
        public void LoadAssetBundles(int indexAsset, string bundleURL)
        {
            AssetManagers[indexAsset] = AssetBundle.LoadFromFile(bundleURL);
            UnitConsole.PrintDebug(AssetManagers[indexAsset] != null ? "AssetBundleLoader (48): Loading Assets[" + indexAsset.ToString() + "] Success" : "AssetBundleLoader (64):  Loading Assets[" + indexAsset.ToString() + "] Fail");
        }

        public void InstantiateObjects(string assetName, Vector3 position)
        {
            UnitConsole.PrintDebug("AssetBundleLoader(52): Load Asset Array: " + assetName);

            for (int i = 0; i < AssetManagers.Length; i++)
            {
                if (AssetManagers[i].Contains(assetName))
                {
                    var prefab = AssetManagers[i].LoadAsset(assetName);
                    if (prefab)
                    {
                        var NewObject = Instantiate(prefab, position, Quaternion.Euler(new Vector3(0, 0, 0)));

                        //-- SET DYNAMIC ARRAY COLLECTION
                        AssetsCollection.Add(new CAssetsCollection());

                        NewObject.name = GlobalID.UserName + "." + GlobalID.UserDesc + "." + (AssetsCollection.Count).ToString() + "." + assetName;

                        AssetsCollection[AssetsCollection.Count - 1] = new CAssetsCollection();
                        AssetsCollection[AssetsCollection.Count - 1].isEnabled = true;
                        AssetsCollection[AssetsCollection.Count - 1].AssetName = NewObject.name;
                        AssetsCollection[AssetsCollection.Count - 1].AssetObject = (GameObject) NewObject;
                    }
                    else
                    {
                        UnitConsole.PrintDebug("AssetBundleLoader(95): Loading Array Object[" + i.ToString() + "] Error - Prefab is NULL");
                    }
                }
            }
        }

        public void InstantiateObjects(string assetName, Vector3 position, Quaternion rotation)
        {
            UnitConsole.PrintDebug("AssetBundleLoader(115): Instantiate Array Assets : " + assetName);
            for (int i = 0; i < AssetManagers.Length; i++)
            {
                if (AssetManagers[i].Contains(assetName))
                {
                    var prefab = AssetManagers[i].LoadAsset(assetName);
                    if (prefab)
                    {
                        var NewObject = Instantiate(prefab, position, rotation);

                        //-- SET DYNAMIC ARRAY COLLECTION
                        AssetsCollection.Add(new CAssetsCollection());

                        NewObject.name = GlobalID.UserName + "." + GlobalID.UserDesc + "." + (AssetsCollection.Count).ToString() + "." + assetName;

                        AssetsCollection[AssetsCollection.Count - 1] = new CAssetsCollection();
                        AssetsCollection[AssetsCollection.Count - 1].isEnabled = true;
                        AssetsCollection[AssetsCollection.Count - 1].AssetName = NewObject.name;
                        AssetsCollection[AssetsCollection.Count - 1].AssetObject = (GameObject)NewObject;
                    }
                    else
                    {
                        UnitConsole.PrintDebug("AssetBundleLoader(127): Loading Array Object[" + i.ToString() + "] Error - Prefab is NULL");
                    }
                }
            }
        }

        public void LoadObjectsFromData(string assetName, Vector3 position, Quaternion rotation)
        {
            UnitConsole.PrintDebug("AssetBundleLoader(115): Instantiate Array Assets : " + assetName);

            string[] objTemp = assetName.Split('.');
            string objOwner = objTemp[0] + "." + objTemp[1] + "." + objTemp[2] + ".";
            string objName = objTemp[3] + "." + objTemp[4] + "." + objTemp[5];

            for (int i = 0; i < AssetManagers.Length; i++)
            {
                if (AssetManagers[i].Contains(objName))
                {
                    var prefab = AssetManagers[i].LoadAsset(objName);
                    if (prefab)
                    {
                        var NewObject = Instantiate(prefab, position, rotation);

                        //-- SET DYNAMIC ARRAY COLLECTION
                        AssetsCollection.Add(new CAssetsCollection());

                        NewObject.name = assetName;

                        AssetsCollection[AssetsCollection.Count - 1] = new CAssetsCollection();
                        AssetsCollection[AssetsCollection.Count - 1].isEnabled = true;
                        AssetsCollection[AssetsCollection.Count - 1].AssetName = NewObject.name;
                        AssetsCollection[AssetsCollection.Count - 1].AssetObject = (GameObject)NewObject;
                    }
                    else
                    {
                        UnitConsole.PrintDebug("AssetBundleLoader(127): Loading Array Object[" + i.ToString() + "] Error - Prefab is NULL");
                    }
                }
            }
        }

        public void Shutdown(object args)
        {
            for (int i=0; i<AssetsCollection.Count; i++)
            {
                if (AssetsCollection[i].AssetName == args.ToString())
                {
                    AssetsCollection.Remove(AssetsCollection[i]);
                    GameObject temp = GameObject.Find(args.ToString());
                    Destroy(temp);
                }
            }
        }

    }
}
