﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace IMedia9
{

    public class BundlesBuilder : Editor
    {
        [MenuItem("Assets/Build Garuda Data")]
        static void BuildAllAssetsBundle()
        {
            string AssetsBundleFolder = Application.streamingAssetsPath + "\\Garuda";
            if (!Directory.Exists(AssetsBundleFolder))
            {
                Directory.CreateDirectory(AssetsBundleFolder);
            }

            BuildPipeline.BuildAssetBundles(
                AssetsBundleFolder, 
                BuildAssetBundleOptions.ChunkBasedCompression,
                BuildTarget.StandaloneWindows64);
        }

    }
}
