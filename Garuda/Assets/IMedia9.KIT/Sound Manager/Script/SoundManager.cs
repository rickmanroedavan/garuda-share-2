﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk suara
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class SoundManager : MonoBehaviour
    {
        public enum CAudioType { PlayByTime, PlayByTrigger }
        public bool isEnabled;
        public CAudioType AudioType;

        [Header("Audio Settings")]
        public AudioSource audioSource;

        [Header("Delay Settings")]
        public bool usingDelay;
        public int Delay;

        // Use this for initialization
        void Start()
        {
            if (isEnabled)
            {
                if (usingDelay)
                {
                    Invoke("PlaySound", Delay);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void PlaySound()
        {
            if (isEnabled) audioSource.Play();
        }

        public void StopSound()
        {
            if (isEnabled) audioSource.Stop();
        }

        public void PauseSound()
        {
            if (isEnabled) audioSource.Pause();
        }

        public void UnPauseSound()
        {
            if (isEnabled) audioSource.UnPause();
        }

        public void Shutdown()
        {
            if (isEnabled) audioSource.Stop();
        }
    }

}
