﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampung nilai global variabel
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class GlobalTime : MonoBehaviour
    {
        public enum CTimeCalculation { Increment, Decrement } 
        public bool isEnabled;
        public int CurrentTime;

        [Header("Time Settings")]
        public CTimeCalculation TimeCalculation;

        [Header("Constraint")]
        public int MinValue;
        public int MaxValue;

        // Use this for initialization
        void Start()
        {
            if (isEnabled)
            {
                if (TimeCalculation == CTimeCalculation.Increment)
                {
                    InvokeRepeating("ExecuteIncrement", 1, 1);
                }
                if (TimeCalculation == CTimeCalculation.Decrement)
                {
                    InvokeRepeating("ExecuteDecrement", 1, 1);
                }
            }
        }

        void ExecuteIncrement()
        {
            CurrentTime = CurrentTime + 1;
            if (CurrentTime >= MaxValue)
            {
                isEnabled = false;
                CancelInvoke();
            }
        }

        void ExecuteDecrement()
        {
            CurrentTime = CurrentTime - 1;
            if (CurrentTime <= MinValue)
            {
                isEnabled = false;
                CancelInvoke();
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        public int GetCurrentTimer()
        {
            return CurrentTime;
        }

        public void SetCurrentTime(int aValue)
        {
            CurrentTime = aValue;
            if (CurrentTime >= MaxValue) CurrentTime = MaxValue;
            if (CurrentTime <= MinValue) CurrentTime = MinValue;
        }

        public void AddToCurrentTime(int aValue)
        {
            CurrentTime += aValue;
            if (CurrentTime >= MaxValue) CurrentTime = MaxValue;
        }

        public void SubstractFromCurrentTime(int aValue)
        {
            CurrentTime -= aValue;
            if (CurrentTime <= MinValue) CurrentTime = MinValue;
        }

        public bool IsShutdown()
        {
            return CurrentTime <= 0;
        }

    }

}
