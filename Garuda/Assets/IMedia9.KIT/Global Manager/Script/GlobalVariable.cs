﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Deklarasi Global Variable untuk 7 variabel standar
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalVariable : MonoBehaviour
    {

        public enum CVariableType { timeVar, healthVar, scoreVar, intVar, floatVar, stringVar, boolVar };
        public CVariableType VariableType;
        public GlobalTime TimeVariables;
        public GlobalHealth HealthVariables;
        public GlobalScore ScoreVariables;
        public GlobalIntVar IntVariables;
        public GlobalFloatVar FloatVariables;
        public GlobalStringVar StringVariables;
        public GlobalBoolVar BoolVariables;

    }

}