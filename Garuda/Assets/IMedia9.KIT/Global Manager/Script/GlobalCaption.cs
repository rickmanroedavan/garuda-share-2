﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampilkan nilai variabel
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class GlobalCaption : MonoBehaviour
    {
        public enum CTimeFormat { None, SS, MMSS, HHMMSS }
        [Space(10)]
        public bool isEnabled;

        [Header("Global Variable")]
        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeVariables;
        public GlobalHealth HealthVariables;
        public GlobalScore ScoreVariables;
        public GlobalIntVar IntVariables;
        public GlobalFloatVar FloatVariables;
        public GlobalStringVar StringVariables;
        public GlobalBoolVar BoolVariables;

        [Header("Debug Variable")]
        public bool usingDebug;
        public string DebugText;

        [Header("UI Variable")]
        public bool usingUI;
        public string PrefixText;
        public Text CaptionText;

        [Header("Time Format")]
        public bool usingTimeFormat;
        public CTimeFormat TimeFormat;
        public string PrefixTime;
        public Text CaptionTime;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (usingDebug)
            {
                if (VariableType == GlobalVariable.CVariableType.timeVar)
                {
                    DebugText = TimeVariables.CurrentTime.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.healthVar)
                {
                    DebugText = HealthVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.scoreVar)
                {
                    DebugText = ScoreVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.intVar)
                {
                    DebugText = IntVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.floatVar)
                {
                    DebugText = FloatVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.stringVar)
                {
                    DebugText = StringVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.boolVar)
                {
                    DebugText = BoolVariables.CurrentValue.ToString();
                }
            }
            if (usingUI)
            {
                if (VariableType == GlobalVariable.CVariableType.timeVar)
                {
                    CaptionText.text = PrefixText + TimeVariables.CurrentTime.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.healthVar)
                {
                    CaptionText.text = PrefixText + HealthVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.scoreVar)
                {
                    CaptionText.text = PrefixText + ScoreVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.intVar)
                {
                    CaptionText.text = PrefixText + IntVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.floatVar)
                {
                    CaptionText.text = PrefixText + FloatVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.stringVar)
                {
                    CaptionText.text = PrefixText + StringVariables.CurrentValue.ToString();
                }
                if (VariableType == GlobalVariable.CVariableType.boolVar)
                {
                    CaptionText.text = PrefixText + BoolVariables.CurrentValue.ToString();
                }
            }
            if (usingTimeFormat)
            {
                if (TimeFormat == CTimeFormat.None)
                {
                    CaptionTime.text = PrefixTime + FloatToTime(TimeVariables.GetCurrentTimer(), "None");
                }
                if (TimeFormat == CTimeFormat.SS)
                {
                    CaptionTime.text = PrefixTime + FloatToTime(TimeVariables.GetCurrentTimer(), "SS");
                }
                if (TimeFormat == CTimeFormat.MMSS)
                {
                    CaptionTime.text = PrefixTime + FloatToTime(TimeVariables.GetCurrentTimer(), "MMSS");
                }
                if (TimeFormat == CTimeFormat.HHMMSS)
                {
                    CaptionTime.text = PrefixTime + FloatToTime(TimeVariables.GetCurrentTimer(), "HHMMSS");
                }
            }

        }

        public string FloatToTime(float toConvert, string format)
        {
            switch (format)
            {
                case "None":
                    return string.Format("{0:00}",
                        Mathf.Floor(toConvert) //seconds
                        );//miliseconds

                case "SS":
                    return string.Format("{0:00}",
                        Mathf.Floor(toConvert) % 60//seconds
                        );//miliseconds

                case "MMSS":
                    return string.Format("{0:00}:{1:00}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60//seconds
                        );//miliseconds

                case "HHMMSS":
                    return string.Format("{0:00}:{1:00}:{2:00}",
                        Mathf.Floor(toConvert / 3600),//hours
                        Mathf.Floor(toConvert / 60) - (Mathf.Floor(toConvert / 3600) * 60), //minutes
                        Mathf.Floor(toConvert) % 60//seconds
                        );//miliseconds

                case "00.0":
                    return string.Format("{0:00}:{1:0}:{2:0}",
                        Mathf.Floor(toConvert) / 3600,//seconds
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 10) % 10));//miliseconds

                case "#0.0":
                    return string.Format("{0:#0}:{1:0}",
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 10) % 10));//miliseconds

                case "00.00":
                    return string.Format("{0:00}:{1:00}",
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 100) % 100));//miliseconds

                case "00.000":
                    return string.Format("{0:00}:{1:000}",
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 1000) % 1000));//miliseconds

                case "#00.000":
                    return string.Format("{0:#00}:{1:000}",
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 1000) % 1000));//miliseconds

                case "#0:00":
                    return string.Format("{0:#0}:{1:00}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60);//seconds

                case "#00:00":
                    return string.Format("{0:#00}:{1:00}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60);//seconds

                case "0:00.0":
                    return string.Format("{0:0}:{1:00}.{2:0}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 10) % 10));//miliseconds

                case "#0:00.0":
                    return string.Format("{0:#0}:{1:00}.{2:0}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 10) % 10));//miliseconds

                case "0:00.00":
                    return string.Format("{0:0}:{1:00}.{2:00}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 100) % 100));//miliseconds

                case "#0:00.00":
                    return string.Format("{0:#0}:{1:00}.{2:00}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 100) % 100));//miliseconds

                case "0:00.000":
                    return string.Format("{0:0}:{1:00}.{2:000}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 1000) % 1000));//miliseconds

                case "#0:00.000":
                    return string.Format("{0:#0}:{1:00}.{2:000}",
                        Mathf.Floor(toConvert / 60),//minutes
                        Mathf.Floor(toConvert) % 60,//seconds
                        Mathf.Floor((toConvert * 1000) % 1000));//miliseconds

            }
            return "error";
        }

    }
}
