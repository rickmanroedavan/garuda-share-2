﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalFloatVar : MonoBehaviour
    {

        public float CurrentValue;

        [Header("Constraint")]
        public int MinValue;
        public int MaxValue;

        void Start()
        {
        }

        public float GetCurrentValue()
        {
            return CurrentValue;
        }

        public void SetCurrentValue(float aValue)
        {
            CurrentValue = aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public void AddToCurrentValue(float aValue)
        {
            CurrentValue += aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
        }

        public void SubToCurrentValue(float aValue)
        {
            CurrentValue -= aValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public bool IsShutdown()
        {
            return CurrentValue <= 0;
        }

    }
}