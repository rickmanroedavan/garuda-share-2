﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menampung nilai global variabel
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalIntVar : MonoBehaviour
    {

        public int CurrentValue;

        [Header("Constraint")]
        public int MinValue;
        public int MaxValue;

        public int GetCurrentValue()
        {
            return CurrentValue;
        }

        public void SetCurrentValue(int aValue)
        {
            CurrentValue = aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public void AddToCurrentValue(int aValue)
        {
            CurrentValue += aValue;
            if (CurrentValue >= MaxValue) CurrentValue = MaxValue;
        }

        public void SubstractFromCurrentValue(int aValue)
        {
            CurrentValue -= aValue;
            if (CurrentValue <= MinValue) CurrentValue = MinValue;
        }

        public bool IsShutdown()
        {
            return CurrentValue <= 0;
        }

    }
}