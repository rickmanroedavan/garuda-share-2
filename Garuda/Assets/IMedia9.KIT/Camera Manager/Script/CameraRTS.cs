﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera ala game jadul Age of Empires, yaitu kamera isometric
 *          kurang lebih 45 derajat ke bawah
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class CameraRTS : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;

        [System.Serializable]
        public class CKeyboardController
        {
            public bool isEnabled;
            [Header("Primary Keys")]
            public KeyCode ForwardKey = KeyCode.UpArrow;
            public KeyCode LeftKey = KeyCode.LeftArrow;
            public KeyCode BackwardKey = KeyCode.DownArrow;
            public KeyCode RightKey = KeyCode.RightArrow;
            public KeyCode UpKey = KeyCode.PageUp;
            public KeyCode DownKey = KeyCode.PageDown;

            [Header("Alternative Keys")]
            public KeyCode AltForwardKey = KeyCode.W;
            public KeyCode AltLeftKey = KeyCode.A;
            public KeyCode AltBackwardKey = KeyCode.S;
            public KeyCode AltRightKey = KeyCode.D;
            public KeyCode AltUpKey = KeyCode.None;
            public KeyCode AltDownKey = KeyCode.None;
        }

        [Header("Keyboard Settings")]
        public CKeyboardController KeyboardController;

        [Header("Special Settings")]
        public KeyCode ResetKey = KeyCode.F5;
        public float MoveSpeed = 100;

        Vector3 startPosition;
        Quaternion startRotation;
        Vector3 lastPosition;

        Vector3 targetPosition;
        Quaternion targetRotation;
        float targetRotationY;
        float targetRotationX;

        void SyncronizeInput()
        {
            targetPosition = TargetCamera.transform.position;
            targetRotation = TargetCamera.transform.rotation;
            targetRotationY = TargetCamera.transform.localRotation.eulerAngles.y;
            targetRotationX = TargetCamera.transform.localRotation.eulerAngles.x;
        }

        // Use this for initialization
        void Start()
        {
            startPosition = TargetCamera.transform.position;
            startRotation = TargetCamera.transform.rotation;

            SyncronizeInput();
        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled)
            {
                if (Input.GetKey(ResetKey))
                {
                    TargetCamera.transform.position = startPosition;
                    TargetCamera.transform.rotation = startRotation;
                }

                //-- Keyboard Controller
                if (KeyboardController.isEnabled)
                {
                    if (Input.GetKey(KeyboardController.ForwardKey) || Input.GetKey(KeyboardController.AltForwardKey))
                    {
                        TargetCamera.transform.position += (Vector3.forward * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.BackwardKey) || Input.GetKey(KeyboardController.AltBackwardKey))
                    {
                        TargetCamera.transform.position += (Vector3.back * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.LeftKey) || Input.GetKey(KeyboardController.AltLeftKey))
                    {
                        TargetCamera.transform.position += (Vector3.left * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.RightKey) || Input.GetKey(KeyboardController.AltRightKey))
                    {
                        TargetCamera.transform.position += (Vector3.right * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.UpKey) || Input.GetKey(KeyboardController.AltUpKey))
                    {
                        TargetCamera.transform.position += (Vector3.up * MoveSpeed * Time.deltaTime);
                    }
                    if (Input.GetKey(KeyboardController.DownKey) || Input.GetKey(KeyboardController.DownKey))
                    {
                        TargetCamera.transform.position += (Vector3.down * MoveSpeed * Time.deltaTime);
                    }
                }

            }
        }

        void LateUpdate()
        {
            SyncronizeInput();
        }
    }
}

