﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin perubahan posisi musuh/teman. Idenya cukup sederhana yaitu dengan 
 *          membandingkan posisi variabel karakter utama, teman dan musuh dengan urutan Ranged, Melee dan Follow.
 *          Jadi fungsi yang duluan di eksekusi adalah fungsi pencarian untuk menyerang, baru follow 
 *          dan patroli. Semua fungsi ditandai dengan variabel debug biar gampang ngecek masing2 statusnya.
 **************************************************************************************************************/

using UnityEngine;
using System.Collections;

namespace IMedia9
{

    public class AIMechanim3D : MonoBehaviour
    {
        public enum CAIType { Friend, Foe } 
        public bool isEnabled;
        public CharacterController AIController;

        public CAIType AIType;

        [Header("Movement Settings")]
        public float MoveSpeed = 20;
        public float RotateSpeed = 5;
        public float jumpSpeed = 8.0F;
        public float gravity = 20.0F;
        private Vector3 moveDirection = Vector3.zero;
        public bool freezeX;
        [Space(10)]

        [Header("Ranged Attack Settings")]
        public bool usingRangedAttack;
        public string RangedTargetTag;
        public float RangedSearchingRange = 100;
        public float RangedAttackRange = 20;
        public int RangedAttackDelay;
        public AIShooter3D Shooter3D;
        bool isInvokeAttack = false;
        [Space(10)]

        [Header("Melee Attack Settings")]
        public bool usingMeleeAttack;
        public string MeleeTargetTag;
        public float MeleeSearchingRange = 100;
        public float MeleeAttackRange = 20;
        [Space(10)]

        [Header("Follow Settings")]
        public bool usingTargetFollow;
        public string TargetTagToFollow;
        public float FollowSearchingRange = 100;
        public float FollowRange = 20;
        [Space(10)]

        [Header("Retreat Settings")]
        public bool usingTargetRetreat;
        public string TargetTagToRetreat;
        public float RetreatSearchingRange = 100;
        public float RetreatRange = 20;
        [Space(10)]

        [Header("Patrol Settings")]
        public bool usingPatrol;
        public GameObject[] PatrolPoints;
        public float PatrolCheckPoint = 10;
        int PatrolIndex; 
        [Space(10)]

        [Header("Debug Value")]
        public bool isMeleeAttack = false;
        public bool isRangedAttack = false;
        public bool isChasing = false;
        public bool isFollow = false;
        public bool isRetreat = false;
        public bool isPatrol = false;
        public bool isGrounded = false;
        public float VectorDistance;

        GameObject RetreatTargetObject;
        GameObject FollowedTargetObject;
        GameObject PlayerTargetObject;
        bool statusPatrolStart = true;

        void Start()
        {
            if (usingPatrol) isPatrol = true;
        }

        void Update()
        {
            if (isEnabled)
            {
                isGrounded = AIController.isGrounded;
                if (AIController.isGrounded)
                {

                    //== RANGED ATTACK ==================================================================================== 
                    if (usingRangedAttack && (AIType == CAIType.Friend || AIType == CAIType.Foe))
                    {

                        PlayerTargetObject = GameObject.FindGameObjectWithTag(RangedTargetTag);
                        if (PlayerTargetObject != null)
                        {
                            if (IsOnRangedAttackSearchingRange())
                            {
                                isChasing = true;
                                isPatrol = false;

                                AIController.transform.LookAt(PlayerTargetObject.transform);

                                moveDirection = AIController.transform.forward;
                                moveDirection *= MoveSpeed;
                                moveDirection.y -= gravity * Time.deltaTime;
                                AIController.Move(moveDirection * Time.deltaTime);
                            }
                            else
                            {
                                isChasing = false;
                                if (usingPatrol) isPatrol = true;
                            }

                            if (IsOnRangedAttackRange())
                            {
                                isRangedAttack = true;

                                AIController.transform.LookAt(PlayerTargetObject.transform);

                                if (!isInvokeAttack)
                                {
                                    isInvokeAttack = true;
                                    InvokeRepeating("ExecAIShooting", 1, RangedAttackDelay);
                                }
                                isPatrol = false;
                            }
                            else
                            {
                                isRangedAttack = false;
                                isInvokeAttack = false;
                                CancelInvoke();
                                if (usingPatrol) isPatrol = true;
                            }
                        }
                    }

                    if (!isChasing && !isRangedAttack)
                    {
                        if (usingPatrol)
                        {
                            if (statusPatrolStart && isPatrol)
                            {
                                PatrolIndex = 0;
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    statusPatrolStart = false;
                                    PatrolIndex++;
                                }
                            }
                            else
                            if (!statusPatrolStart && isPatrol && PatrolIndex < PatrolPoints.Length)
                            {
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    PatrolIndex++;
                                }
                            }
                            if (!statusPatrolStart && isPatrol && PatrolIndex >= PatrolPoints.Length)
                            {
                                PatrolIndex = 0;
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    statusPatrolStart = true;
                                }
                            }

                            moveDirection = AIController.transform.forward;
                            moveDirection *= MoveSpeed;
                            moveDirection.y -= gravity * Time.deltaTime;
                            AIController.Move(moveDirection * Time.deltaTime);
                        }
                    }

                    //== MELEE ATTACK ==================================================================================== 
                    if (usingMeleeAttack && (AIType == CAIType.Friend || AIType == CAIType.Foe))
                    {

                        PlayerTargetObject = GameObject.FindGameObjectWithTag(MeleeTargetTag);
                        if (PlayerTargetObject != null)
                        {
                            if (IsOnMeleeAttackSearchingRange())
                            {
                                isChasing = true;
                                isPatrol = false;

                                AIController.transform.LookAt(PlayerTargetObject.transform);

                                moveDirection = AIController.transform.forward;
                                moveDirection *= MoveSpeed;
                                moveDirection.y -= gravity * Time.deltaTime;
                                AIController.Move(moveDirection * Time.deltaTime);
                            }
                            else
                            {
                                isChasing = false;
                                if (usingPatrol) isPatrol = true;
                            }

                            if (IsOnMeleeAttackRange())
                            {
                                isMeleeAttack = true;
                                isPatrol = false;
                            }
                            else
                            {
                                isMeleeAttack = false;
                                isInvokeAttack = false;
                                CancelInvoke();
                                if (usingPatrol) isPatrol = true;
                            }
                        }
                    }

                    if (!isChasing && !isMeleeAttack)
                    {
                        if (usingPatrol)
                        {
                            if (statusPatrolStart && isPatrol)
                            {
                                PatrolIndex = 0;
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    statusPatrolStart = false;
                                    PatrolIndex++;
                                }
                            }
                            else
                            if (!statusPatrolStart && isPatrol && PatrolIndex < PatrolPoints.Length)
                            {
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    PatrolIndex++;
                                }
                            }
                            if (!statusPatrolStart && isPatrol && PatrolIndex >= PatrolPoints.Length)
                            {
                                PatrolIndex = 0;
                                AIController.transform.LookAt(PatrolPoints[PatrolIndex].transform);
                                VectorDistance = Vector3.Distance(AIController.transform.position, PatrolPoints[PatrolIndex].transform.position);
                                if (VectorDistance < PatrolCheckPoint)
                                {
                                    statusPatrolStart = true;
                                }
                            }

                            moveDirection = AIController.transform.forward;
                            moveDirection *= MoveSpeed;
                            moveDirection.y -= gravity * Time.deltaTime;
                            AIController.Move(moveDirection * Time.deltaTime);
                        }

                    }

                    //== ALLY RETREAT  ==================================================================================== 
                    if (usingTargetRetreat && AIType == CAIType.Friend && !isChasing && !isMeleeAttack && !isRangedAttack)
                    {
                        RetreatTargetObject = GameObject.FindGameObjectWithTag(TargetTagToRetreat);
                        if (RetreatTargetObject != null)
                        {
                            if (IsOnRetreatSearchingRange())
                            {
                                isRetreat = true;

                                AIController.transform.LookAt(RetreatTargetObject.transform);
                                AIController.transform.localEulerAngles = new Vector3(AIController.transform.localEulerAngles.x,
                                                                                      AIController.transform.localEulerAngles.y+180,
                                                                                      AIController.transform.localEulerAngles.z);

                                moveDirection = AIController.transform.forward;
                                moveDirection *= MoveSpeed;
                                moveDirection.y -= gravity * Time.deltaTime;
                                AIController.Move(moveDirection * Time.deltaTime);
                            }
                            else
                            {
                                AIController.transform.LookAt(RetreatTargetObject.transform);
                                isRetreat = false;
                            }
                        }
                    }

                    //== ALLY FOLLOW  ==================================================================================== 
                    if (usingTargetFollow && AIType == CAIType.Friend && !isChasing && !isMeleeAttack && !isRangedAttack)
                    {
                        FollowedTargetObject = GameObject.FindGameObjectWithTag(TargetTagToFollow);
                        if (FollowedTargetObject != null)
                        {
                            if (IsOnFollowSearchingRange())
                            {
                                isFollow = true;

                                AIController.transform.LookAt(FollowedTargetObject.transform);

                                moveDirection = AIController.transform.forward;
                                moveDirection *= MoveSpeed;
                                moveDirection.y -= gravity * Time.deltaTime;
                                AIController.Move(moveDirection * Time.deltaTime);
                            }
                            else
                            {
                                isFollow = false;
                            }
                        }
                    }


                }
                else
                {
                    moveDirection.y -= gravity * Time.deltaTime;
                    AIController.Move(moveDirection * Time.deltaTime);
                }

                if (freezeX)
                {
                    AIController.transform.localEulerAngles = new Vector3(0, AIController.transform.localEulerAngles.y, AIController.transform.localEulerAngles.z);
                }
            }
        }

        bool IsOnMeleeAttackSearchingRange()
        {
            bool result = false;

            if (PlayerTargetObject != null)
            {
                VectorDistance = Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position);
                result = (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < MeleeSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) > MeleeAttackRange);

            }

            return result;
        }

        bool IsOnMeleeAttackRange()
        {
            bool result = false;

            if (PlayerTargetObject != null)
            {
                result = (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < MeleeSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < MeleeAttackRange);
            }

            return result;
        }

        bool IsOnRangedAttackSearchingRange()
        {
            bool result = false;

            if (PlayerTargetObject != null)
            {
                VectorDistance = Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position);
                result = (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < RangedSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) > RangedAttackRange);

            }

            return result;
        }

        bool IsOnRangedAttackRange()
        {
            bool result = false;

            if (PlayerTargetObject != null)
            {
                result = (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < RangedSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, PlayerTargetObject.transform.position) < RangedAttackRange);
            }

            return result;
        }


        bool IsOnFollowSearchingRange()
        {
            bool result = false;

            if (FollowedTargetObject != null)
            {
                VectorDistance = Vector3.Distance(AIController.transform.position, FollowedTargetObject.transform.position);
                result = (Vector3.Distance(AIController.transform.position, FollowedTargetObject.transform.position) < FollowSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, FollowedTargetObject.transform.position) > FollowRange);

            }

            return result;
        }

        bool IsOnRetreatSearchingRange()
        {
            bool result = false;

            if (RetreatTargetObject != null)
            {
                VectorDistance = Vector3.Distance(AIController.transform.position, RetreatTargetObject.transform.position);
                result = (Vector3.Distance(AIController.transform.position, RetreatTargetObject.transform.position) < RetreatSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, RetreatTargetObject.transform.position) > RetreatRange);

            }

            return result;
        }

        bool IsOnFollowRange()
        {
            bool result = false;

            if (FollowedTargetObject != null)
            {
                result = (Vector3.Distance(AIController.transform.position, FollowedTargetObject.transform.position) < FollowSearchingRange) &&
                         (Vector3.Distance(AIController.transform.position, FollowedTargetObject.transform.position) < FollowRange);
            }

            return result;
        }


        public bool GetMovingStatus()
        {
            return isChasing || isPatrol || isFollow || isRetreat;
        }

        public bool GetAttackStatus()
        {
            return isMeleeAttack || isRangedAttack;
        }

        public void ExecAIShooting()
        {
            Shooter3D.ExecShooter();
        }

        void Shutdown(bool aValue)
        {
            isEnabled = false;
        }

    }
}