﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk perubahan animasi karakter menggunakan Keyboard
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class Anima3DShutdown : MonoBehaviour
    {

        public enum CCompareType { Greater, Equal, Less }
        public enum CParameterType { Int, Float, Bool, Trigger }

        public bool isEnabled;
        public Animator TargetAnimator;

        [System.Serializable]
        public class CShutdownState3D
        {
            [Header("Shutdown Settings")]
            public string StateNow;
            public string StateNext;
            public CParameterType ParameterType;
            public string ParameterName;
            public string PositiveValue;
            public string NegativeValue;
            public KeyCode[] ShutdownTestkey;
        }

        public CShutdownState3D ShutdownState3D;

        [Header("Sound Settings")]
        public bool usingSound;
        public AudioSource animaAudioSource;
        public AudioClip animaAudioClip;

        [Header("Disable Input Settings")]
        public bool DisabledInputWhenDeath;
        public Mechanim3DKeyboard InputKeyboard;
        public Mechanim3DMouse InputMouse;
        public Mechanim3DDpad InputDPad;

        void Update()
        {
            if (isEnabled)
            {
                for (int i = 0; i < ShutdownState3D.ShutdownTestkey.Length; i++)
                {
                    if (Input.GetKey(ShutdownState3D.ShutdownTestkey[i]))
                    {
                        Shutdown(true);
                    }
                }
            }

        }

        void Shutdown(bool aValue)
        {
            if (ShutdownState3D.ParameterType == CParameterType.Float)
            {
                float dummyvalue = float.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetFloat(ShutdownState3D.ParameterName, dummyvalue);
                ExecuteSound();
            }
            if (ShutdownState3D.ParameterType == CParameterType.Int)
            {
                int dummyvalue = int.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetInteger(ShutdownState3D.ParameterName, dummyvalue);
                ExecuteSound();
            }
            if (ShutdownState3D.ParameterType == CParameterType.Bool)
            {
                bool dummyvalue = bool.Parse(ShutdownState3D.PositiveValue);
                TargetAnimator.SetBool(ShutdownState3D.ParameterName, dummyvalue);
                ExecuteSound();
            }
            if (ShutdownState3D.ParameterType == CParameterType.Trigger)
            {
                TargetAnimator.SetTrigger(ShutdownState3D.ParameterName);
                ExecuteSound();
            }

            if (DisabledInputWhenDeath)
            {
                if (InputKeyboard != null)
                {
                    InputKeyboard.enabled = false;
                }
                if (InputMouse != null)
                {
                    InputMouse.enabled = false;
                }
                if (InputDPad != null)
                {
                    InputDPad.enabled = false;
                }
            }
        }

        void ExecuteSound(bool plays = true)
        {
            if (usingSound)
            {
                if (!animaAudioSource.isPlaying && plays)
                {
                    animaAudioSource.clip = animaAudioClip;
                    animaAudioSource.Play();
                }
                else if (!plays)
                {
                    animaAudioSource.clip = animaAudioClip;
                    animaAudioSource.Stop();
                }
            }
        }
    }

}