﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk manage button yang muncul di bag
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class BagManager : MonoBehaviour
    {
        public bool isEnabled;

        [System.Serializable]
        public class CTargetPanel
        {
            [HideInInspector]
            public string ItemTagName;
            public Image IconPosition;
            public bool isUsed;
        }

        [System.Serializable]
        public class CIconStatus
        {
            public string ItemTagName;
            public Image IconImage;
            [HideInInspector]
            public int IconSlot;
        }

        [Header("Container Settings")]
        public CTargetPanel[] TargetPanel;

        [Header("Icon Status")]
        public CIconStatus[] IconStatus;

        [Header("Debug Settings")]
        public bool isDebug;
        public KeyCode TriggerKey;
        public string DebugReceiverObjectName;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (isDebug)
            {
                if (Input.GetKeyUp(TriggerKey))
                {
                    GetItemThenShowIcon(DebugReceiverObjectName);
                }
            }
        }

        public void GetItemThenShowIcon(string TagName)
        {
            for (int i = 0; i < IconStatus.Length; i++)
            {
                if (IconStatus[i].ItemTagName == TagName)
                {
                    for (int j = 0; j < TargetPanel.Length; j++)
                    {
                        if (!TargetPanel[j].isUsed)
                        {
                            TargetPanel[j].isUsed = true;
                            TargetPanel[j].ItemTagName = IconStatus[i].ItemTagName;

                            IconStatus[i].IconSlot = j;
                            IconStatus[i].IconImage.gameObject.SetActive(true);
                            IconStatus[i].IconImage.transform.position = TargetPanel[j].IconPosition.transform.position;
                            break;
                        }
                    }
                }
            }
        }

        public void UseThisIcon(string ItemTagName)
        {
           for (int j = 0; j < TargetPanel.Length; j++)
           {
               if (TargetPanel[j].ItemTagName == ItemTagName)
               {
                   TargetPanel[j].isUsed = false;
                   TargetPanel[j].ItemTagName = "";
               }
           }
           for (int j = 0; j < IconStatus.Length; j++)
           {
               if (IconStatus[j].ItemTagName == ItemTagName)
               {
                    IconStatus[j].IconImage.gameObject.SetActive(false);
               }
           }
        }

    }
}
