﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk testing nilai variabel
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class UIHealthText : MonoBehaviour
    {

        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeVariables;
        public GlobalHealth HealthVariables;
        public GlobalScore ScoreVariables;
        public GlobalIntVar IntVariables;
        public GlobalFloatVar FloatVariables;
        public GlobalStringVar StringVariables;
        public GlobalBoolVar BoolVariables;

        [Header("Display Settings")]
        public Text DisplayText;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            switch (VariableType)
            {
                case GlobalVariable.CVariableType.timeVar:
                    DisplayText.text = TimeVariables.GetCurrentTimer().ToString();
                    break;
                case GlobalVariable.CVariableType.healthVar:
                    DisplayText.text = HealthVariables.GetCurrentValue().ToString();
                    break;
                case GlobalVariable.CVariableType.scoreVar:
                    DisplayText.text = ScoreVariables.GetCurrentValue().ToString();
                    break;
                case GlobalVariable.CVariableType.intVar:
                    DisplayText.text = IntVariables.GetCurrentValue().ToString();
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    DisplayText.text = FloatVariables.GetCurrentValue().ToString();
                    break;
                case GlobalVariable.CVariableType.stringVar:
                    DisplayText.text = StringVariables.GetCurrentValue();
                    break;
                case GlobalVariable.CVariableType.boolVar:
                    DisplayText.text = BoolVariables.GetCurrentValue().ToString();
                    break;
            }

        }
    }

}