﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar pergerakan dalam Unity yang terdiri dari Position, Rotation, & Scale.
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedPosition : MonoBehaviour
{

    public enum CScriptFor { ThisGameObject, AnotherGameObject }

    public CScriptFor ScriptFor;
    public GameObject AnotherGameObject;

    public Vector3 ObjectPosition;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (ScriptFor == CScriptFor.ThisGameObject)
        {
            this.transform.position += ObjectPosition;
        } else if (ScriptFor == CScriptFor.AnotherGameObject)
        {
            AnotherGameObject.transform.position += ObjectPosition;
        }
    }
}
