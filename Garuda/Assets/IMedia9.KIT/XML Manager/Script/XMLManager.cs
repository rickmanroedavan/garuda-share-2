﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;

namespace IMedia9
{

    public class XMLManager : MonoBehaviour
    {

        public string AssetLoaderName;
        public string GUIMainCanvas;
        [HideInInspector]
        public GameObject AssetLoaderObject;
        [HideInInspector]
        public GameObject GUIMainCanvasObject;

        [System.Serializable]
        public class CAssetsBundleData {
            public string FileName;
            public int TotalData;
            public GameObject[] PrefabObject;
            public string[] PrefabGroup;
            public string[] PrefabName;
            public string[] PrefabCaption;
        }

        [Header("Dialog Settings")]
        public GameObject Content;
        public GameObject PanelDynamic;
        public GameObject UnitPanelObject;

        [Header("Image Settings")]
        public Sprite[] ButtonImage;

        [Header("XML Settings")]
        public string FileName = "AssetsConfig.xml";
        public CAssetsBundleData[] AssetsBundleData;

        [Header("Status Settings")]
        public bool isLoaded = false;

        // Use this for initialization
        void Start()
        {
            LoadConfigXML();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public string ConfigDirectory()
        {
            return Application.streamingAssetsPath + "/Config/";
        }

        public string DataDirectory()
        {
            return Application.streamingAssetsPath + "/Data/";
        }

        public void SetGroupVisible(string GroupName)
        {
            for (int i=0; i < AssetsBundleData.Length; i++)
            {
                for (int j = 0; j < AssetsBundleData[i].PrefabName.Length; j++)
                {
                    string[] group = AssetsBundleData[i].PrefabName[j].Split('.');
                    if (group[0] != GroupName)
                    {
                        AssetsBundleData[i].PrefabObject[j].SetActive(false);
                        UnitConsole.PrintDebug("XMLManager (65): UnLoad " + "Panel_" + AssetsBundleData[i].PrefabName[j]);
                    } else
                    if (group[0] == GroupName)
                    {
                        AssetsBundleData[i].PrefabObject[j].SetActive(true);
                        UnitConsole.PrintDebug("XMLManager (70): Load " + "Panel_" + AssetsBundleData[i].PrefabName[j]);
                    }
                }
            }
        }

        public void SaveDataXML(string aFileName)
        {
            AssetLoaderObject = GameObject.Find(AssetLoaderName);
            GUIMainCanvasObject = GameObject.Find(GUIMainCanvas);

            var sr = File.CreateText(DataDirectory() + aFileName + ".xml");
            sr.WriteLine("<DataCollection>");

            sr.WriteLine("<DataGroup>");
            sr.WriteLine("\t<Value>");
            sr.WriteLine("\t\t" + AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection.Count.ToString());
            sr.WriteLine("</Value>");
            sr.WriteLine("\t</DataGroup>");

            for (int i=0; i<AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection.Count; i++)
            {
                sr.WriteLine("\t<Data" + i.ToString()+">");
                sr.WriteLine("\t\t<Name>" + AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.name + "</Name>");
                sr.WriteLine("\t\t<Position>" + AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.position.x.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.position.y.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.position.z.ToString() + ";" +
                                           "</Position>");
                sr.WriteLine("\t\t<Rotation>" + AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localRotation.eulerAngles.x.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localRotation.eulerAngles.y.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localRotation.eulerAngles.z.ToString() + ";" +
                                           "</Rotation>");
                sr.WriteLine("\t\t<Scale>" + AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localScale.x.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localScale.y.ToString() + ";" +
                                           AssetLoaderObject.GetComponent<AssetBundleLoader>().AssetsCollection[i].AssetObject.transform.localScale.z.ToString() + ";" +
                                           "</Scale>");
                sr.WriteLine("\t</Data" + i.ToString() + ">");
            }

            sr.WriteLine("</DataCollection>");
            sr.Close();

            GUIMainCanvasObject.GetComponent<MenuStatusCaption>().SetStatusCaption("File " + aFileName + ".xml telah disimpan.");
        }

        public void LoadDataXML(string aFileName)
        {
            AssetLoaderObject = GameObject.Find(AssetLoaderName);
            GUIMainCanvasObject = GameObject.Find(GUIMainCanvas);

            string xmlfile = System.IO.File.ReadAllText(DataDirectory() + aFileName + ".xml");
            UnitConsole.PrintDebug("XMLManager (135): Processing: " + aFileName + ".xml");

            if (File.Exists(DataDirectory() + aFileName + ".xml"))
            {

                XmlDocument xmldoc;
                XmlNodeList xmlnodelist;
                XmlNode xmlnode;
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlfile);

                int total = 0;
                xmlnodelist = xmldoc.GetElementsByTagName("DataGroup");
                for (int i = 0; i <= xmlnodelist.Count - 1; i++)
                {
                    xmlnode = xmlnodelist.Item(i);
                    total = int.Parse(xmlnode.FirstChild.InnerText);
                }

                for (int i = 0; i < total; i++)
                {
                    xmlnodelist = xmldoc.GetElementsByTagName("Data" + i.ToString());

                    xmlnode = xmlnodelist.Item(0);
                    XmlNode currentNode = xmlnode.FirstChild;
                    UnitConsole.PrintDebug("XMLManager (154): Load Data" + i.ToString() + ": " + currentNode.InnerText);

                    string objName = currentNode.InnerText;

                    currentNode = currentNode.NextSibling;
                    UnitConsole.PrintDebug("XMLManager (160): Load Position" + i.ToString() + ": " + currentNode.InnerText);

                    string[] strPosition = currentNode.InnerText.Split(';');
                    Vector3 tempPosition = new Vector3(float.Parse(strPosition[0]), float.Parse(strPosition[1]), float.Parse(strPosition[2]));

                    currentNode = currentNode.NextSibling;
                    UnitConsole.PrintDebug("XMLManager (163): Load Rotation" + i.ToString() + ": " + currentNode.InnerText);

                    string[] strRotation = currentNode.InnerText.Split(';');
                    Vector3 tempRotation = new Vector3(float.Parse(strRotation[0]), float.Parse(strRotation[1]), float.Parse(strRotation[2]));
                    Quaternion quaRotation = new Quaternion();
                    quaRotation.eulerAngles = tempRotation;

                    currentNode = currentNode.NextSibling;
                    UnitConsole.PrintDebug("XMLManager (166): Load Scale" + i.ToString() + ": " + currentNode.InnerText);

                    AssetLoaderObject.GetComponent<AssetBundleLoader>().LoadObjectsFromData(objName, tempPosition, quaRotation);
                }
            } else
            {
                GUIMainCanvasObject.GetComponent<MenuStatusCaption>().SetStatusCaption("File " + aFileName + ".xml berhasil dibuka.");
            }
        }

        public void LoadConfigXML()
        {
            string xmlfile = System.IO.File.ReadAllText(ConfigDirectory() + FileName);
            XmlDocument xmldoc;
            XmlNodeList xmlnodelist;
            XmlNode xmlnode;
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlfile);

            int total = 0;
            xmlnodelist = xmldoc.GetElementsByTagName("AssetDataGroup");
            for (int i = 0; i <= xmlnodelist.Count - 1; i++)
            {
                xmlnode = xmlnodelist.Item(i);
                total = int.Parse(xmlnode.FirstChild.InnerText);
            }

            AssetsBundleData = new CAssetsBundleData[total];

            for (int i = 0; i < total; i++)
            {
                xmlnodelist = xmldoc.GetElementsByTagName("AssetGroup" + i.ToString());
                UnitConsole.PrintDebug("XMLManager (98): Load AssetGroup: " + "AssetGroup" + i.ToString());

                    xmlnode = xmlnodelist.Item(0);
                    XmlNode currentNode = xmlnode.FirstChild;

                    AssetsBundleData[i] = new CAssetsBundleData();
                    AssetsBundleData[i].FileName = currentNode.InnerText;
                    UnitConsole.PrintDebug("XMLManager (105): Load FileName" + i.ToString() +": " + AssetsBundleData[i].FileName);

                    currentNode = currentNode.NextSibling;

                    AssetsBundleData[i].TotalData = int.Parse(currentNode.InnerText);
                    AssetsBundleData[i].PrefabName = new string[AssetsBundleData[i].TotalData];
                    AssetsBundleData[i].PrefabGroup = new string[AssetsBundleData[i].TotalData];
                    AssetsBundleData[i].PrefabCaption = new string[AssetsBundleData[i].TotalData];
                    AssetsBundleData[i].PrefabObject = new GameObject[AssetsBundleData[i].TotalData];

                    Content.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(
                            RectTransform.Axis.Vertical,
                            Content.GetComponent<RectTransform>().rect.height * (1 + Mathf.RoundToInt(AssetsBundleData[i].TotalData / 4)));

                    for (int k = 0; k <= AssetsBundleData[i].TotalData - 1; k++)
                    {
                        currentNode = currentNode.NextSibling;
                        string temp = currentNode.InnerText;

                        GameObject temp_button = Instantiate(UnitPanelObject, PanelDynamic.transform);

                        AssetsBundleData[i].PrefabName[k] = temp;
                        AssetsBundleData[i].PrefabGroup[k] = temp.Split('.')[0];
                        AssetsBundleData[i].PrefabCaption[k] = temp.Split('.')[2];
                        AssetsBundleData[i].PrefabObject[k] = temp_button;

                        temp_button.name = "Panel_" + AssetsBundleData[i].PrefabName[k];
                        temp_button.transform.GetChild(0).name = "Button_" + AssetsBundleData[i].PrefabName[k];
                        temp_button.transform.GetChild(0).transform.GetChild(0).name = "Toggle_" + AssetsBundleData[i].PrefabName[k];

                        string panel_name = "Panel." + AssetsBundleData[i].PrefabName[k];
                        string button_name = "Button." + AssetsBundleData[i].PrefabName[k];
                        string toggle_name = "Toggle." + AssetsBundleData[i].PrefabName[k];

                        temp_button.GetComponent<UnitGroup>().UnitSetPanelName(panel_name);
                        temp_button.GetComponent<UnitGroup>().UnitSetButtonName(button_name);
                        temp_button.GetComponent<UnitGroup>().UnitSetToggleName(toggle_name);

                        temp_button.GetComponent<UnitGroup>().UnitSetCaption(AssetsBundleData[i].PrefabCaption[k], k);
                        temp_button.GetComponent<UnitGroup>().UnitSetXMLDataCaption(AssetsBundleData[i].PrefabName[k]);

                        for (int x=0; x < ButtonImage.Length; x++)
                        {
                            if (ButtonImage[x].name == AssetsBundleData[i].PrefabName[k])
                            {
                                temp_button.GetComponent<UnitGroup>().UnitSetButtonImage(ButtonImage[x]);
                            }
                        }
                    }

            }

            isLoaded = true;

        }

    }
}

