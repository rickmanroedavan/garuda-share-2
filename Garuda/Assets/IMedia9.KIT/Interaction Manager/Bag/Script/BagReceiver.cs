﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk manage item yang ditabrak terus dimasukin ke tas
 **************************************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace IMedia9
{
    public class BagReceiver : MonoBehaviour
    {
        public enum COperator { AddValue, SetValue, SubValue }

        [System.Serializable]
        public class CBagItemCollection
        {
            public string BagItemSenderTagName;
            public int BagItemIndex;
            public float BagItemValue;
            public int BagItemTotal;
        }

        [Header("Bag Receiver Settings")]
        public string BagReceiverTagName;
        public Rigidbody RigidbodyObject;
        [Space(10)]

        [Header("Bag Manager Settings")]
        public string BagManagerObjectName;
        [HideInInspector]
        public GameObject BagManagerObject;

        [Header("Item Identification")]
        public string[] BagItemTagName;

        [Header("Item Collection")]
        public CBagItemCollection[] BagItemCollection;
        [HideInInspector]
        public CBagItemCollection GetBagItemCollection;
        int GetExistIndex;
        float ItemReceived;
        bool isExistItem;
        string ColliderObjectName;

        [Header("Refresh Rate")]
        public float RefreshRate = 0.2f;

        // Use this for initialization
        void Start()
        {
            BagManagerObject = GameObject.Find(BagManagerObjectName);
            if (!BagManagerObject)
            {
                Debug.Log("IMedia9: Woi, bro! Did you forget to put BagReceiverObject Prefab?");
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            bool isVerified = false; 

            for (int i=0; i< BagItemTagName.Length; i++)
            {
                if (BagItemTagName[i] == collider.tag)
                {
                    isVerified = true;
                }
            }

            if (isVerified)
            {
                ColliderObjectName = collider.gameObject.name;
                GameObject temp = GameObject.Find(ColliderObjectName);

                if (temp.GetComponent<ItemSender>() == null)
                {
                    Debug.Log("IMedia9: Woi, bro! Did you forget to attach an ItemSender script?");
                }

                float temp_Value = temp.GetComponent<ItemSender>().ItemValue;
                COperator temp_operator = (COperator)temp.GetComponent<ItemSender>().ItemOperator;
                ItemReceived = temp_Value;

                for (int i = 0; i < BagItemTagName.Length; i++)
                {
                    if (BagItemTagName[i] == collider.tag)
                    {
                        if (BagItemCollection.Length == 0)
                        {
                            BagItemCollection = new CBagItemCollection[BagItemCollection.Length + 1];

                            GetBagItemCollection.BagItemIndex = 0;
                            GetBagItemCollection.BagItemSenderTagName = BagItemTagName[i];
                            GetBagItemCollection.BagItemValue = ItemReceived;
                            GetBagItemCollection.BagItemTotal += 1;

                            Invoke("AddNewItem", RefreshRate);
                        }
                        else
                        {
                            isExistItem = false;
                            for (int j = 0; j < BagItemCollection.Length; j++)
                            {
                                if (BagItemTagName[i] == BagItemCollection[j].BagItemSenderTagName)
                                {
                                    isExistItem = true;
                                    GetExistIndex = j;

                                    GetBagItemCollection.BagItemIndex = j;
                                    GetBagItemCollection.BagItemSenderTagName = BagItemTagName[i];
                                    GetBagItemCollection.BagItemValue = ItemReceived;
                                    GetBagItemCollection.BagItemTotal += 1;

                                }
                            }

                            if (!isExistItem)
                            {
                                Array.Resize<CBagItemCollection>(ref BagItemCollection, BagItemCollection.Length + 1);

                                GetBagItemCollection.BagItemIndex = 0;
                                GetBagItemCollection.BagItemSenderTagName = BagItemTagName[i];
                                GetBagItemCollection.BagItemValue = ItemReceived;
                                GetBagItemCollection.BagItemTotal += 1;

                                Invoke("AddNewItem", RefreshRate);
                            }
                        }

                    }
                }

            }
        }

        void AddNewItem()
        {
            int lastIndex = BagItemCollection.Length - 1;

            BagItemCollection[lastIndex] = new CBagItemCollection();

            if (BagItemCollection[lastIndex] != null)
            {
                BagItemCollection[lastIndex].BagItemSenderTagName = GetBagItemCollection.BagItemSenderTagName;
                BagItemCollection[lastIndex].BagItemIndex = GetBagItemCollection.BagItemIndex;
                BagItemCollection[lastIndex].BagItemValue = GetBagItemCollection.BagItemValue;
                BagItemCollection[lastIndex].BagItemTotal = GetBagItemCollection.BagItemTotal;

                Debug.Log("IMedia9: AddNewItem = Success");

                BagManagerObject.GetComponent<BagManager>().GetItemThenShowIcon(GetBagItemCollection.BagItemSenderTagName);

                GameObject temp = GameObject.Find(ColliderObjectName);
                bool temp_destroy = temp.GetComponent<ItemSender>().DestroyAfterCollision;
                if (temp_destroy)
                {
                    Destroy(temp);
                }

            }
            else
            {
                Debug.Log("Index: Null");
            }
        }

        void AddExistItem()
        {
            if (BagItemCollection[GetExistIndex] != null)
            {
                BagItemCollection[GetExistIndex].BagItemSenderTagName = GetBagItemCollection.BagItemSenderTagName;
                BagItemCollection[GetExistIndex].BagItemIndex = GetBagItemCollection.BagItemIndex;
                BagItemCollection[GetExistIndex].BagItemValue += GetBagItemCollection.BagItemValue;
                BagItemCollection[GetExistIndex].BagItemTotal += GetBagItemCollection.BagItemTotal;
            }
        }

    }
}
