﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk perubahan posisi objek non-karakter menggunakan Translate
 **************************************************************************************************************/

namespace IMedia9
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class MenuManager : MonoBehaviour
    {
        public bool isEnabled;

        [Header("Menu Settings")]
        public KeyCode TriggerKey = KeyCode.Escape;
        public GameObject MainSidebar;
        public GameObject[] MainContainer;

        [Header("Menu Settings Extended")]
        public GameObject[] MainContainerExtended;

        [Header("StatusBar Settings")]
        public InputField MouseXY;
        public InputField CoordXYZ;

        // Use this for initialization
        void Start()
        {
            HideAllMainContainer();
            MainSidebar.SetActive(true);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (isEnabled)
            {
                if (Input.GetKey(TriggerKey))
                {
                    HideAllMainContainer();
                }
                if (Input.GetKey(KeyCode.Mouse0) && Input.mousePosition.x > 300)
                {
                    HideAllMainContainer();
                }

                MouseXY.text = GlobalMouse.GetStringMouseXY();
                CoordXYZ.text = GlobalMouse.GetStringCoordXYZ();
            }
        }

        public void HideAllMainContainer()
        {
            for (int i=0; i<MainContainer.Length; i++)
            {
                MainContainer[i].SetActive(false);
                
            }
        }

        public void HideAllMainContainerExtended()
        {
            for (int i = 0; i < MainContainerExtended.Length; i++)
            {
                MainContainerExtended[i].SetActive(false);

            }
        }

        public void ShowMainContainer(GameObject MainContainer)
        {
            if (isEnabled)
            {
                MainContainer.SetActive(true);
            }
        }

    }
}
