﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuStatusCaption : MonoBehaviour
{

    public static string StaticStatusCaption;
    public InputField StatusCaption;

    [Header("Activate On Start")]
    public GameObject[] ActivateObject;

    [Header("Deactivate On Start")]
    public GameObject[] DeactivateObject;

    // Start is called before the first frame update
    void Start()
    {
        for (int i=0; i<ActivateObject.Length; i++)
        {
            ActivateObject[i].SetActive(true);
        }
        for (int i = 0; i < DeactivateObject.Length; i++)
        {
            DeactivateObject[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetStatusCaption(string aValue)
    {
        StatusCaption.text = aValue;
    }

    public void ClearStatusCaption()
    {
        StatusCaption.text = "";
    }

    public static void SetStatusBarCaption(string aValue)
    {
        StaticStatusCaption = aValue;
    }

}
