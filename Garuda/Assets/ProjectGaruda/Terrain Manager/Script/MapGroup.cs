﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGroup : MonoBehaviour {

    public enum CMapType { NormalMap, RoadMap, TerrainMap, SatelliteMap, HybridMap }
    public enum CCameraType { PrespectiveCamera, OrthogonalCamera }

    public bool isEnabled;
    public CMapType MapType;
    public CCameraType CameraType;

    [System.Serializable]
    public class CZoomGroup
    {
        public float DistanceMin;
        public float DistanceMax;
        public Material MapMaterial;
    }

    [System.Serializable]
    public class CMapGroups
    {
        public GameObject MapPlane;
        public int CurrentDistanceIndex;

        [Header("Normal Map")]
        public bool usingNormalMap;
        public CZoomGroup[] NormalMap;

        [Header("Road Map")]
        public bool usingRoadMap;
        public CZoomGroup[] RoadMap;

        [Header("Terrain Map")]
        public bool usingTerrainMap;
        public CZoomGroup[] TerrainMap;

        [Header("Satellite Map")]
        public bool usingSatelliteMap;
        public CZoomGroup[] SatelliteMap;

        [Header("HybridMap Map")]
        public bool usingHybridMap;
        public CZoomGroup[] HybridMap;
    }

    [Header("Zoom Checking Settings")]
    public string CameraPivotName;
    public GameObject MapPivot;

    [Header("MapNormal Settings")]
    public bool usingMapNormal;
    public float DistanceMinNormal;
    public float DistanceMaxNormal;
    public GameObject MapGroupNormal;

    [Header("MapDetail Settings")]
    public bool usingMapDetail;
    public float DistanceMinDetail;
    public float DistanceMaxDetail;
    public GameObject MapGroupDetail;
    public CMapType MapDetailType;

    [HideInInspector]
    public GameObject CameraPivot;
    [HideInInspector]
    public int CurrentIndex = -1;
    [HideInInspector]
    public int LastCurrentIndex = -999;
    [HideInInspector]
    public CMapType LastMapType;

    [Header("Map Group Settings")]
    public CMapGroups[] MapGroups;

    // Use this for initialization
    void Start () {
        CameraPivot = GameObject.Find(CameraPivotName) as GameObject;
        MapGroupDetail.SetActive(false);
        InvokeRepeating("SlowUpdate", 1, 1);
	}
	
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha1))
        {
            ActivateNormapMap();
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha2))
        {
            ActivateRoadMap(); 
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha3))
        {
            ActivateTerrainMap();
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha4))
        {
            ActivateSatelliteMap();
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Alpha5))
        {            ActivateHybridMap();
        }
    }

    void SlowUpdate () {

        float ZoomDistance = 0;

        if (CameraType == CCameraType.PrespectiveCamera)
        {
            ZoomDistance = CameraPivot.transform.position.y - MapPivot.transform.position.y;
        }
        if (CameraType == CCameraType.OrthogonalCamera)
        {
            ZoomDistance = CameraPivot.GetComponent<Camera>().orthographicSize - MapPivot.transform.position.y;
        }

        if (usingMapNormal)
        {
            if (DistanceMinNormal < ZoomDistance && ZoomDistance <= DistanceMaxNormal)
            {
                if (!MapGroupNormal.activeSelf && !MapGroupDetail.activeSelf)
                    MapGroupNormal.SetActive(true);
            } else
            {
                if (MapGroupNormal.activeSelf)
                    MapGroupNormal.SetActive(false);
            }
        }

        if (usingMapDetail)
        {
            if (MapType == MapDetailType)
            {
                if (DistanceMinDetail < ZoomDistance && ZoomDistance <= DistanceMaxDetail)
                {
                    if (!MapGroupDetail.activeSelf)
                    {
                        MapGroupDetail.SetActive(true);
                        MapGroupNormal.SetActive(false);
                    }
                }
                else
                {
                    if (MapGroupDetail.activeSelf)
                    {
                        MapGroupDetail.SetActive(false);
                        MapGroupNormal.SetActive(true);
                    }
                }
            }
        }

        if (MapGroupNormal.activeSelf)
        {
            for (int i = 0; i < MapGroups.Length; i++)
            {
                if (MapGroups[i].usingNormalMap && MapType == CMapType.NormalMap)
                {
                    for (int j = 0; j < MapGroups[i].NormalMap.Length; j++)
                    {
                        if (MapGroups[i].NormalMap[j].DistanceMin <= ZoomDistance && ZoomDistance <= MapGroups[i].NormalMap[j].DistanceMax)
                        {
                            MapGroups[i].CurrentDistanceIndex = j;
                            if (MapGroups[i].MapPlane.GetComponent<Renderer>().material != MapGroups[i].NormalMap[j].MapMaterial)
                                MapGroups[i].MapPlane.GetComponent<Renderer>().material = MapGroups[i].NormalMap[j].MapMaterial;
                        }
                    }
                }
                if (MapGroups[i].usingTerrainMap && MapType == CMapType.TerrainMap)
                {
                    for (int j = 0; j < MapGroups[i].TerrainMap.Length; j++)
                    {
                        if (MapGroups[i].TerrainMap[j].DistanceMin <= ZoomDistance && ZoomDistance <= MapGroups[i].TerrainMap[j].DistanceMax)
                        {
                            MapGroups[i].CurrentDistanceIndex = j;
                            if (MapGroups[i].MapPlane.GetComponent<Renderer>().material != MapGroups[i].TerrainMap[j].MapMaterial)
                                MapGroups[i].MapPlane.GetComponent<Renderer>().material = MapGroups[i].TerrainMap[j].MapMaterial;
                        }
                    }
                }
                if (MapGroups[i].usingRoadMap && MapType == CMapType.RoadMap)
                {
                    for (int j = 0; j < MapGroups[i].RoadMap.Length; j++)
                    {
                        if (MapGroups[i].RoadMap[j].DistanceMin <= ZoomDistance && ZoomDistance <= MapGroups[i].RoadMap[j].DistanceMax)
                        {
                            MapGroups[i].CurrentDistanceIndex = j;
                            if (MapGroups[i].MapPlane.GetComponent<Renderer>().material != MapGroups[i].RoadMap[j].MapMaterial)
                                MapGroups[i].MapPlane.GetComponent<Renderer>().material = MapGroups[i].RoadMap[j].MapMaterial;
                        }
                    }
                }
                if (MapGroups[i].usingSatelliteMap && MapType == CMapType.SatelliteMap)
                {
                    for (int j = 0; j < MapGroups[i].SatelliteMap.Length; j++)
                    {
                        if (MapGroups[i].SatelliteMap[j].DistanceMin <= ZoomDistance && ZoomDistance <= MapGroups[i].SatelliteMap[j].DistanceMax)
                        {
                            MapGroups[i].CurrentDistanceIndex = j;
                            if (MapGroups[i].MapPlane.GetComponent<Renderer>().material != MapGroups[i].SatelliteMap[j].MapMaterial)
                                MapGroups[i].MapPlane.GetComponent<Renderer>().material = MapGroups[i].SatelliteMap[j].MapMaterial;
                        }
                    }
                }
                if (MapGroups[i].usingHybridMap && MapType == CMapType.HybridMap)
                {
                    for (int j = 0; j < MapGroups[i].HybridMap.Length; j++)
                    {
                        if (MapGroups[i].HybridMap[j].DistanceMin <= ZoomDistance && ZoomDistance <= MapGroups[i].HybridMap[j].DistanceMax)
                        {
                            MapGroups[i].CurrentDistanceIndex = j;
                            if (MapGroups[i].MapPlane.GetComponent<Renderer>().material != MapGroups[i].HybridMap[j].MapMaterial)
                                MapGroups[i].MapPlane.GetComponent<Renderer>().material = MapGroups[i].HybridMap[j].MapMaterial;
                        }
                    }
                }
            }
        }

    }

    public void ActivateNormapMap()
    {
        MapType = CMapType.NormalMap;
    }

    public void ActivateHybridMap()
    {
        MapType = CMapType.HybridMap;
    }

    public void ActivateRoadMap()
    {
        MapType = CMapType.RoadMap;
    }

    public void ActivateSatelliteMap()
    {
        MapType = CMapType.SatelliteMap;
    }

    public void ActivateTerrainMap()
    {
        MapType = CMapType.TerrainMap;
    }

}
