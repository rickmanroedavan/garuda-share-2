﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class UnitXMLButton : MonoBehaviour
    {

        public string InputFieldName;
        public Button ThisButton;
        GameObject InputFieldObject;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetCaptionButton()
        {
            InputFieldObject = GameObject.Find(InputFieldName);
            InputFieldObject.GetComponent<InputField>().text = ThisButton.GetComponentInChildren<Text>().text;
        }
    }
}

