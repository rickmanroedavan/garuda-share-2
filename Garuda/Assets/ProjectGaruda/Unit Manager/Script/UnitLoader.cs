﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace IMedia9
{
    public class UnitLoader : MonoBehaviour
    {

        public string ToggleGroupTag;

        [Header("Panel Caption")]
        public string[] ArrayCaption;
        public Text PanelObjectCaption;

        [Header("Asset Bundle Settings")]
        public string AssetsDataName;
        GameObject AssetsDataObject;

        public void SetCaption(int aValue)
        {
            PanelObjectCaption.text = ArrayCaption[aValue];
        }

        // Use this for initialization
        void Start()
        {
            AssetsDataObject = GameObject.Find(AssetsDataName);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Mouse0) && GlobalMouse.MouseType == GlobalMouse.CMouseType.MousePlacement)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    LoadObject();
                }
            }
        }

        public void ActivateToggleButton(Toggle toggleButton)
        {
            GameObject[] groupButton = GameObject.FindGameObjectsWithTag(ToggleGroupTag);

            for (int i = 0; i < groupButton.Length; i++)
            {
                groupButton[i].GetComponent<Toggle>().isOn = false;
            }

            toggleButton.isOn = true;
        }

        public void LoadObject()
        {

            GameObject[] groupButton = GameObject.FindGameObjectsWithTag(ToggleGroupTag);

            for (int i = 0; i < groupButton.Length; i++)
            {
                if (groupButton[i].GetComponent<Toggle>().isOn)
                {
                    string[] objectname = groupButton[i].name.Split('_');
                    AssetsDataObject.GetComponent<AssetBundleLoader>().InstantiateObjects(objectname[1], GlobalMouse.GetVectorCoordXYZ()); 
                }
            }

        }

        public void LoadObjectByName(string aName, GameObject aPosition)
        {
            AssetsDataObject.GetComponent<AssetBundleLoader>().InstantiateObjects(aName, aPosition.transform.position);
        }
    }
}
