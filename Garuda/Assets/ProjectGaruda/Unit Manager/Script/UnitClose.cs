﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class UnitClose : MonoBehaviour
    {

        public GameObject UnitGroupPanel;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void CloseUnitPanel()
        {
            UnitGroupPanel.SetActive(false);
        }

        public void OffSceeenUnitPanel()
        {
            UnitGroupPanel.transform.position = new Vector3(0, -Screen.height, 0);
        }

        public void OnSceeenUnitPanel()
        {
            UnitGroupPanel.transform.position = new Vector3(Screen.width/2, Screen.height/2, 0);
        }
    }
}
