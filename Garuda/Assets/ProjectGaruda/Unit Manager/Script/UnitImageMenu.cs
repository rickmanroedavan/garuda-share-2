﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class UnitImageMenu : MonoBehaviour
{
    public bool isEnabled;
    public GameObject ImageButton;
    public GameObject ImageButtonParent;

    [Header("Image GameObject")]
    public GameObject ImageObject;

    [Header("Extension Settings")]
    public string[] FileExtension;


    // Start is called before the first frame update
    void Start()
    {
        ImageObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            ImageObject.SetActive(false);
        }
    }

    public bool isValid(string aFileExt)
    {
        bool result = false;
        for (int i=0; i<FileExtension.Length; i++)
        {
            if (FileExtension[i] == aFileExt)
            {
                result = true;
            }
        }
        return result;
    }

    public string ImageDirectory()
    {
        return Application.streamingAssetsPath + "/Image/";
    }

    public void CloseImage()
    {
        ImageObject.SetActive(false);
    }

    public void ShowImageFile()
    {
        if (isEnabled)
        {

            DirectoryInfo tempDir;
            FileInfo[] tempFile = null;
            if (Directory.Exists(ImageDirectory()))
            {
                tempDir = new DirectoryInfo(ImageDirectory());
                tempFile = tempDir.GetFiles();
            }

            for (int i = 0; i < ImageButtonParent.transform.childCount; i++)
            {
                Destroy(ImageButtonParent.transform.GetChild(i).gameObject);
            }

            Vector2 contentWidth = new Vector2(0, 100); // ImageButtonParent.GetComponent<RectTransform>().sizeDelta;

            for (int i = 0; i < tempFile.Length; i++)
            {
                string temp = Path.GetExtension(tempFile[i].FullName);
                if (isValid(temp))
                {
                    GameObject tempButton = Instantiate(ImageButton, ImageButtonParent.transform);
                    tempButton.GetComponentInChildren<Text>().text = tempFile[i].Name;

                    contentWidth.y += 50;
                    ImageButtonParent.GetComponent<RectTransform>().sizeDelta = contentWidth;
                }
            }

        }
    }
}
