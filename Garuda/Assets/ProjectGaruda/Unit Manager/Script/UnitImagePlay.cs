﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitImagePlay : MonoBehaviour
{

    public string imagePanelName;
    [HideInInspector]
    public GameObject imagePanelObject;
    [HideInInspector]
    public GameObject imagePanel;
    public Material imageMaterial;

    // Start is called before the first frame update
    void Start()
    {
        imagePanelObject = GameObject.Find(imagePanelName);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayImage(Button imageNameFromButton)
    {

        imagePanelObject.transform.GetChild(0).gameObject.SetActive(true);
        imagePanel = imagePanelObject.transform.GetChild(0).gameObject;

        string urlImage = Application.streamingAssetsPath + "/Image/" + imageNameFromButton.GetComponentInChildren<Text>().text;

        if (File.Exists(urlImage))
        {
            var bytes = System.IO.File.ReadAllBytes(urlImage);
            var tex = new Texture2D(1, 1);
            tex.LoadImage(bytes);
            imageMaterial.mainTexture = tex;

            MeshRenderer mr = imagePanel.GetComponent<MeshRenderer>();
            mr.material = imageMaterial;
        }
    }

}
