﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class UnitGroup : MonoBehaviour
    {

        public string ToggleGroupTag;
        public Text UnitGroupText;
        public GameObject UnitGroupButton;
        GameObject UnitGroupPanel;

        [Header("XML Unit Group")]
        public string UnitGroupPanelName;
        public string UnitGroupButtonName;
        public string UnitGroupToggleName;

        [Header("XML Data")]
        public string XMLDataName;

        [Header("Debug Value")]
        public int IndexButton;

        // Use this for initialization
        void Start()
        {
            UnitGroupPanel = GameObject.Find(UnitGroupPanelName);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ActivateToggleButton(Toggle toggleButton)
        {
            GameObject[] groupButton = GameObject.FindGameObjectsWithTag(ToggleGroupTag);

            for (int i = 0; i < groupButton.Length; i++)
            {
                groupButton[i].GetComponent<Toggle>().isOn = false;
            }

            toggleButton.isOn = true;
        }

        public void CloseUnitPanel()
        {
            UnitGroupPanel.SetActive(false);
        }

        public void UnitSetCaption(string aText, int aIndex)
        {
            UnitGroupText.text = aText;
            IndexButton = aIndex;
        }

        public void UnitSetPanelName(string aValue)
        {
            UnitGroupPanelName = aValue;
        }

        public void UnitSetButtonName(string aValue)
        {
            UnitGroupButtonName = aValue;
        }

        public void UnitSetToggleName(string aValue)
        {
            UnitGroupToggleName = aValue;
        }

        public void UnitSetXMLDataCaption(string aDataName)
        {
            XMLDataName = aDataName;
        }

        public void UnitSetButtonImage(Sprite aValue)
        {
            UnitGroupButton.GetComponent<Image>().sprite = aValue;
        }

    }
}

