﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class UnitVisibility : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void CloseThisGameObject(GameObject ThisObject)
        {
            ThisObject.SetActive(false);
        }

        public void OpenThisGameObject(GameObject ThisObject)
        {
            ThisObject.SetActive(true);
            ThisObject.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            ThisObject.transform.SetAsFirstSibling();
        }

        public void CenterGameObject()
        {
            transform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            transform.SetAsFirstSibling();
        }

        public void BringToFront()
        {
            this.transform.SetAsLastSibling();
        }

        public void SendToBack()
        {
            this.transform.SetAsFirstSibling();
        }
    }
}
