﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

    public float Speed;
    public float Gravity;
    public Rigidbody TargetRigidbody;
    public string[] TargetTag;

    [Header("Damage Settings")]
    public bool usingDamage;
    public string[] TargetDamage;

    [Header("SFX Settings")]
    public bool usingTargetSFX;
    public GameObject TargetSFX;

    [Header("Sound Settings")]
    public AudioSource MainAudio;
    public bool usingBeginSound;
    public AudioClip BeginSound;
    public bool usingEndSound;
    public AudioClip EndSound;

    // Use this for initialization
    void Start () {
		if (usingBeginSound)
        {
            MainAudio.clip = BeginSound;
            MainAudio.Play();
        }
	}
	
	// Update is called once per frame
	void Update () {
        TargetRigidbody.velocity = (transform.forward + new Vector3(0, Gravity, 0)) * Speed * Time.deltaTime;
        if (TargetRigidbody.velocity != Vector3.zero) {
            transform.rotation = Quaternion.LookRotation(TargetRigidbody.velocity);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        for (int i = 0; i < TargetTag.Length; i++)
        {
            if (collider.tag == TargetTag[i])
            {
                Speed = 0;

                TargetRigidbody.useGravity = false;
                TargetRigidbody.isKinematic = true;

                if (usingTargetSFX)
                {
                    GameObject temp = Instantiate(TargetSFX, transform.position, transform.rotation);
                    if (usingEndSound)
                    {
                        MainAudio.clip = EndSound;
                        MainAudio.Play();
                    }
                }

                Destroy(this.gameObject, 3f);
            }
        }
        if (usingDamage)
        {
            for (int i = 0; i < TargetDamage.Length; i++)
            {
                if (collider.tag == TargetDamage[i])
                {
                    collider.gameObject.GetComponent<IMedia9.UnitManager>().Shutdown(true);
                }
            }
        }
    }
}
